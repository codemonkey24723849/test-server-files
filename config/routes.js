module.exports.routes = {
  'get /catalog/all-books': 'BookController.find',
  'get /user/borrowed-books': 'BookController.findBorrowedBooks',
  'post /user/borrow-book': 'BookController.borrowBook',
  'post /user/return-book': 'BookController.returnBook'
};
