module.exports = {
  findBorrowedBooks: async function (req, res) {
    try {
      const B = sails.models.book;
      const books = await B.find({ borrowedBy: req.user.id });
      if (!books) throw sails.StatusCode(404);
      res.ok(books);
    } catch (e) {
      res.negotiate(e);
    }
  },
  borrowBook: async function (req, res) {
    try {
      if (!req.body.id) throw sails.StatusCode(400);
      if (req.user.isGuest()) {
        throw sails.StatusCode(401);
      }

      const B = sails.models.book;

      let book = await B.findOne(req.body.id);
      if (!book) throw sails.StatusCode(404);
      if (book.borrowedBy) throw sails.StatusCode(409, 'The book ' + book.id + ' is already borrowed');

      [book] = await B.update(book, { borrowedBy: req.user.id });
      res.ok(book);
      book.borrowedBy = req.user;
      this.addUrls(book);
      B.publishMessage('book-borrowed', book, book, req);
      sails.services.statistics.add('borrow-book', req.user.id, { id: book.id, userId: req.user.id, userName: req.user.username });
    } catch (e) {
      res.negotiate(e);
    }
  },

  returnBook: async function (req, res) {
    try {
      if (!req.body.id) throw sails.StatusCode(400);
      if (req.user.isGuest()) {
        throw sails.StatusCode(401);
      }

      const B = sails.models.book;

      let book = await B.findOne(req.body.id);
      if (!book) throw sails.StatusCode(404);
      if (book.borrowedBy !== req.user.id) throw sails.StatusCode(409, 'You didn\'t borrowed book ' + book.id);

      [book] = await B.update(book, { borrowedBy: null });
      res.ok(book);
      this.addUrls(book);
      B.publishMessage('book-returned', book, book, req);
      sails.services.statistics.add('return-book', req.user.id, { id: book.id, userId: req.user.id, userName: req.user.username });
    } catch (e) {
      res.negotiate(e);
    }
  },

  addUrls: function (obj) {
    const apiUrl = sails.config.apiUrl;
    if (obj.smallThumb)
      obj.smallThumbUrl = `${apiUrl}/file/${obj.smallThumb}`;
    if (obj.mediumThumb)
      obj.mediumThumbUrl = `${apiUrl}/file/${obj.mediumThumb}`;
  }
};
