module.exports = {
  autoCreatedBy: false,
  autoUpdatedAt: false,

  attributes: {
    event: {
      type: 'string',
      required: true
    },
    data: {
      type: 'json'
    },
    user: {
      model: 'User'
    }
  }
};