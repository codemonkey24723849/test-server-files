module.exports = {
  attributes: {
    firstName: {
      type: 'string',
      index: true
    },
    lastName: {
      type: 'string',
      index: true
    }
  }
};
