module.exports = {
  attributes: {
    name: {
      type: 'string',
      index: true,
      required: true
    },
    description: {
      type: 'string'
    },
    smallThumb: {
      model: 'file'
    },
    mediumThumb: {
      model: 'file'
    },
    author: {
      model: 'author'
    },
    borrowedBy: {
      model: 'user'
    },
    toJSON: function () {
      const obj = this.toObject();
      this._addUrls(obj);
      return obj;
    },
    _addUrls: function (obj) {
      const apiUrl = sails.config.apiUrl;
      if (obj.smallThumb)
        obj.smallThumbUrl = `${apiUrl}/file/${obj.smallThumb}`;
      if (obj.mediumThumb)
        obj.mediumThumbUrl = `${apiUrl}/file/${obj.mediumThumb}`;
    }
  }
};
